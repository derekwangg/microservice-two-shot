from django.db import models
from django.urls import reverse
# Create your models here.


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()





class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )


    def get_api_url(self):
        return reverse("api_shoes", kwargs={"id": self.id})


    def __str__(self):
        return self.name
