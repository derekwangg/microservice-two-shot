from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoe, BinVO
from django.http import JsonResponse
import json

# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
        "bin_number",
        "bin_size",
        "id",
        ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "url",
        "bin",
        "id",
    ]
    encoders = {
    "bin": BinVODetailEncoder(),
    }



class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "bin",
        "id",
        "color",
        "url",
    ]
    encoders = {
    "bin": BinVODetailEncoder(),
    }

    def get_extra_data(self, o):
        return {"bin": o.bin.id}




@require_http_methods(["GET", "POST"])
def api_shoes(request, bin_vo_id=None):
    """
    Collection RESTful API handler for Shoe objects in
    the wardrobe.

    GET:
    Returns a dictionary with a single key "shoes" which
    is a list of the shoe name, shoe color, and shoe manufacturer, along with its href and id.

    POST:
    Creates a Shoe resource and returns its details.
    """

    if request.method =="GET":
        if bin_vo_id is not None:
            shoes=Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes=Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder,)
    else: #POST
        content = json.loads(request.body)
        try:
            bin_href = f"/api/bins/{bin_vo_id}/"
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )




@require_http_methods(["DELETE", "GET",])
def api_shoe(request, id):
    """
    Single-object API for the Bin resource.

    GET:
    Returns the information for a Bin resource based
    on the value of id
    DELETE:
    Removes the shoe resource from the application
    """
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Shoe does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            shoe = Shoe.objects.get(id=id)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist, cant delete"})
