import {Link} from 'react-router-dom'
import { useState } from 'react';



function ShoeList(props) {

    const [shoes, setShoes] = useState(props.shoes);
    const ShoeDelete = async (id) => {
        const response = await fetch(`http://localhost:8080/api/shoes/${id}/`, {
        method: 'DELETE',
        });
        if (response.ok) {
        setShoes(shoes.filter(shoe => shoe.id !== id));
        }
    };


    return (
        <><div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Anotha One</Link>
        </div>
        <table className="table table-stipend">
                <thead>
                    <tr>
                        <th> Name </th>
                        <th> Manufacturer </th>
                        <th> Color </th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map(shoe => (
                        <tr key={shoe.id}>
                            <td>{shoe.name}</td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.color}</td>
                            <td><img src ={shoe.url} width="200" height="200"/></td>
                            <td><button onClick={() => ShoeDelete(shoe.id)}>Delete</button></td>
                        </tr>
                    )

                    )};
                </tbody>
            </table></>

    );
}
export default ShoeList;
