import React, { useEffect, useState } from 'react';
function ShoeForm() {
    const [bins, setBins] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        manufacturer: '',
        color: '',
        url: '',
        bin: '',
    })
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setBins(data.bins);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    const handleSubmit = async (event) => {
        event.preventDefault();
        const BinId = formData.bin
        const url = `http://localhost:8080/api/bins/${BinId}/shoes/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        setFormData({
        name: '',
        manufacturer: '',
        color: '',
        url: '',
        bin: '',
        });
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        ...formData,
        [inputName]: value
        });
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name}placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.manufacturer}placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.url}placeholder="Url" required type="url" name="url" id="url" className="form-control" />
                <label htmlFor="url">Url</label>
                </div>
                <div className="mb-3">
                <select onChange={handleFormChange} required name="bin" id="bin" className="form-select" value={formData.bin}>
                    <option value="">Choose a bin</option>
                    {bins.map(bin => {
                    return (
                        <option key={bin.id} value={bin.id}>{bin.id}</option>
                    )
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
    }
export default ShoeForm;
