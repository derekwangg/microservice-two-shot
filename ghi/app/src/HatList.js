import React, { useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

function HatColumn(props) {
  return (
    <div className="col">
      {props.list.map(data => {
        return (
          <div key={data.href} className="card mb-3 shadow">
            <img src={data.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{data.style_name}</h5>
              <h6 className="cart-subtitle mb-2 text-muted"/>
                {data.fabric}
              <h6 className="cart-subtitle mb-2 text-muted"/>
                {data.color}
              <h6 className="cart-subtitle mb-2 text-muted"/>
              Location: {data.location.id}
              <h6 className="cart-subtitle mb-2 text-muted"/>
              Closet: {data.location.closet_name}
              <h6 className="cart-subtitle mb-2 text-muted"/>
              Section: {data.location.section_number}
              <h6 className="cart-subtitle mb-2 text-muted"/>
              Shelf: {data.location.shelf_number}
            </div>
            <div className="card-footer">
              <button onClick={async () =>{
                const hatUrl=`http://localhost:8090/${data.href}`
                const fetchConfig ={
                  method:"delete",
                }
                const response=await fetch(hatUrl, fetchConfig);
                if(response.ok){
                  window.location.reload()
                }
              }}
              className="btn btn-primary">
                Delete hat
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

const HatList = (props) =>  {
  const [hatColumns, setHatColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090${hat.href}`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);

        const columns = [[], [], []];

        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            columns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }

        setHatColumns(columns);
      }
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
        <h1 className="display-5 fw-bold">Your Dope Hats!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            For those overachievers who wear many hats!
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Anotha One</Link>
          </div>
        </div>
      </div>
      <div className="container">
        <h2>Your Collection</h2>
        <div className="row">
          {hatColumns.map((hatList, index) => {
            return (
              <HatColumn key={index} list={hatList} />
            );
          })}
        </div>
      </div>
    </>
  );
}

export default HatList;
