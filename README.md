# Wardrobify

Team:

* Person 1 - Which microservice?
* Brandon Gomez - Shoes
* Derek Wang - Hats Microservice
* Person 2 - Which microservice?

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
1. Creating data models for shoes
2. Create views and Restful APIs that reference models
3. Creating URLs for views
4. Use insomnia for tests for links
5. Set up database (PostgreSQL) to handle POST request
6. create a poller for shoe data from Wardrobe API
## Hats microservice

Create a function view to show list of hats model, configuring the view in a URLS file, including URLs from Hats app into Wardrobe URls.
Adding functionality to handle Post requests to add to database and GET requests.
Build a REACT component to fetch the list and show the list of hats in the web page, route it to the appropriate navbar link.
Build functionality within REACT component to add, view, or delete hats using the REACT UI with the hats microservice backend
Write a poller to obtain bin or location data for the hats microservice using the requests library to make the HTTP request to the Wardrobe API, looping over it and adding to the hats database.
Adding delete functionality through a delete button for each item with a fetch DELETE method using the object ID.
